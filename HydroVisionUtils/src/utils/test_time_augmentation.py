import torch
from typing import List


class Rotate90:
    """
    Rotate images 0/90/180/270 degrees

    Args:
        angles (list): angles to rotate images
        transform (str): Type of transformation, either 'rotate' or 'reverse'.
    """

    identity_param = 0

    def __init__(self, angles: List[int], transform: str = "rotate"):
        self.transform = transform
        self.angles = list(angles)

        if self.identity_param not in angles:
            self.angles.append(self.identity_param)

    @staticmethod
    def rot90(x: torch.Tensor, k: int = 1) -> torch.Tensor:
        """
        Rotate an image by 90 degrees k times.

        Args:
            x (torch.Tensor): Input image tensor of shape (C, H, W).
            k (int): Number of 90-degree rotations. Default is 1.

        Returns:
            torch.Tensor: Rotated image tensor.
        """
        return torch.rot90(x, k, dims=[1, 2])

    def rotate_image(self, image: torch.Tensor, angle: int = 0) -> torch.Tensor:
        """
        Apply rotation augmentation to an image.

        Args:
            image (torch.Tensor): Input image tensor of shape (C, H, W).
            angle (int): Rotation angle in degrees. Default is 0.

        Returns:
            torch.Tensor: Rotated image tensor.
        """
        if image.ndim != 3:
            raise ValueError(
                f"Expected image to have a shape of (C, H, W), got {image.shape}"
            )

        k = angle // 90 if angle >= 0 else (angle + 360) // 90
        return self.rot90(image, k)

    def __call__(self, images: torch.Tensor) -> List[torch.Tensor]:
        """
        Apply rotation transformation to a batch of images.

        Args:
            images (torch.Tensor): Batch of input image tensors of shape (N, C, H, W).

        Returns:
            List[torch.Tensor]: List of rotated image tensors.
        """
        if self.transform == "rotate":
            augmented_images = [
                self.rotate_image(images, angle) for angle in self.angles
            ]
        else:
            augmented_images = [
                self.rotate_image(images[idx], -self.angles[idx])
                for idx in range(len(images))
            ]

        return augmented_images
