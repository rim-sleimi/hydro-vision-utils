import random
from pathlib import Path
from typing import List, Tuple, Dict, Union
import numpy as np


def create_directory(directory_path: str):
    """
    Check if a directory exists; if not, create it.

    Parameters:
    - directory_path (str): Path to the directory to check/create.
    """

    directory_path = (
        Path(directory_path) if not isinstance(directory_path, Path) else directory_path
    )
    directory_path.mkdir(parents=True, exist_ok=True)
    print(
        f"Directory '{directory_path}' {'already exists' if directory_path.exists() else 'created' }."
    )


def get_file_paths(data_dir: Union[Path, str], extension: str = "*.tif") -> List[Path]:
    return sorted(Path(data_dir).glob(extension))


def find_path_by_index(image_paths, pattern):

    for path_str in image_paths:
        path = Path(path_str)
        if pattern in path.name:
            return path
    return None


def split_files(
    files: List[Path], validation_size: int, seed: int = 260122
) -> Tuple[List[Path], List[Path]]:
    random.seed(seed)
    random_indices = random.sample(range(len(files)), validation_size)
    val_files = [files[i] for i in random_indices]
    train_files = [file for idx, file in enumerate(files) if idx not in random_indices]
    return train_files, val_files


def update_band_data(
    band_data: Dict[str, List[np.ndarray]], data: np.ndarray, bands: List[str]
):
    for i, band in enumerate(bands):
        band_data[band].append(data[i])


def compute_statistics(
    band_data: Dict[str, List[np.ndarray]]
) -> Tuple[Dict[str, float], Dict[str, float]]:
    medians = {}
    stds = {}
    for band, data_list in band_data.items():
        stacked_data = np.stack(data_list)
        medians[band] = np.nanmedian(stacked_data, axis=(0, 1, 2))
        stds[band] = np.nanstd(stacked_data, axis=(0, 1, 2))
    return medians, stds
