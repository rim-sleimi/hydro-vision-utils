import cv2
import numpy as np
import matplotlib.pyplot as plt
from typing import List, Tuple


def binary_mask_to_closed_polygons(binary_mask: np.ndarray) -> List[np.ndarray]:
    """
    Convert a binary boundary mask to polygons, considering only closed shapes.

    Args:
        binary_mask (np.ndarray): Binary mask.

    Returns:
        List of closed polygons.
    """
    # Convert the binary mask to uint8
    binary_mask = (binary_mask * 255).astype(np.uint8)

    # Find contours and hierarchy
    contours, hierarchy = cv2.findContours(
        binary_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )

    # Extract closed polygons from contours
    closed_polygons: List[np.ndarray] = []
    for contour in contours:
        # Check if the contour is closed by verifying the length of the contour
        if cv2.contourArea(contour) > 0:  # Filter out very small areas, if necessary
            closed_polygons.append(contour.reshape(-1, 2))

    return closed_polygons
