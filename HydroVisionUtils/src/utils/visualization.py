import matplotlib.pyplot as plt
import torch
from typing import List, Tuple


def create_subplot(rows: int, cols: int, figsize: Tuple[int, int] = (12, 9)):
    fig, axes = plt.subplots(rows, cols, figsize=figsize)
    return fig, axes


def plot_image(ax, image: torch.Tensor, title: str, cmap: str = "gray"):
    ax.imshow(image, cmap=cmap)
    ax.set_title(title)
    ax.axis("off")


def adjust_subplots(wspace: float = 0.1, hspace: float = 0.1):
    plt.subplots_adjust(wspace=wspace, hspace=hspace)


def plot_tta_patches(patches: List[torch.Tensor], output: str):
    fig, axes = create_subplot(2, 2)
    for idx, aug_patch in enumerate(patches):
        row, col = divmod(idx, 2)
        image = aug_patch[0].cpu().numpy()
        title = f"{output} @ {idx * 90}°"
        plot_image(axes[row, col], image, title)

    adjust_subplots()
    plt.show()


def plot_predictions(predictions: List[torch.Tensor]):
    fig, axes = create_subplot(1, 2)

    boundary_image = (
        torch.sum(torch.stack(predictions[0]), dim=0)[0].cpu().numpy() > 0.8
    )
    plot_image(axes[0], boundary_image, "TTA Boundary predictions")

    extent_image = torch.mean(torch.stack(predictions[1]), dim=0)[0].cpu().numpy() > 0.5
    plot_image(axes[1], extent_image, "TTA Extent predictions")

    adjust_subplots()
    plt.show()


def plot_training_sample(
    self, reflectance_data_tensor: torch.Tensor, mask_tensor: torch.Tensor
) -> None:

    # Convert tensors to numpy arrays
    reflectance_data = (
        reflectance_data_tensor[[2, 1, 0], :, :].numpy().transpose(1, 2, 0)
    )
    extent_mask = mask_tensor[0, :, :].numpy()
    boundary_mask = mask_tensor[1, :, :].numpy()
    distance_map = mask_tensor[2, :, :].numpy()

    # Plot reflectance data
    fig, axes = plt.subplots(2, 2, figsize=(12, 12))
    axes[0, 0].imshow(reflectance_data, vmin=0, vmax=1)
    axes[0, 0].set_title("Reflectance Data (RGB)")

    # Plot each component of the mask
    axes[0, 1].imshow(extent_mask, cmap="gray")
    axes[0, 1].set_title("Extent Mask")

    axes[1, 0].imshow(boundary_mask, cmap="gray")
    axes[1, 0].set_title("Boundary Mask")

    axes[1, 1].imshow(distance_map)
    axes[1, 1].set_title("Distance Map")

    plt.show()


# Example usage:
# patches = [torch.randn(1, 64, 64) for _ in range(4)]  # Example patches
# plot_tta_patches(patches, "Example Output")
# predictions = [[torch.randn(1, 64, 64) for _ in range(3)], [torch.randn(1, 64, 64) for _ in range(3)]]
# plot_predictions(predictions)


# def plot_tta_patches(patches: List[torch.Tensor], output: str):
#     """
#     Plot patches in a 2x2 grid.

#     Args:
#         patches (List[torch.Tensor]): List of augmented patches.
#     """
#     fig, axes = plt.subplots(2, 2, figsize=(12, 9))
#     for idx, aug_patch in enumerate(patches):
#         axes[idx // 2, idx % 2].imshow(aug_patch[0].cpu().numpy(), cmap="gray")
#         axes[idx // 2, idx % 2].set_title(f"{output} @ {idx * 90}°")
#         axes[idx // 2, idx % 2].axis("off")

#     # Adjust the spacing between subplots
#     plt.subplots_adjust(wspace=0.1, hspace=0.1)

#     plt.show()


# def plot_predictions(predictions: List[torch.Tensor]):

#     fig, axes = plt.subplots(1, 2, figsize=(12, 9))

#     axes[0].imshow(
#         torch.sum(torch.stack(predictions[0]), dim=0)[0].cpu().numpy() > 0.8,
#         cmap="gray",
#     )
#     axes[0].set_title("TTA Boundary predictions")
#     axes[0].axis("off")
#     axes[1].imshow(
#         torch.mean(torch.stack(predictions[1]), dim=0)[0].cpu().numpy() > 0.5,
#         cmap="gray",
#     )
#     axes[1].set_title("TTA Extent predictions")
#     axes[1].axis("off")

#     plt.subplots_adjust(wspace=0.1, hspace=0.1)
#     plt.show()
