import cv2
import numpy as np
from scipy.ndimage import gaussian_filter, sobel


def inverse_gaussian_gradient(image, sigma=1.0):
    # Compute the gradients
    grad_x = sobel(image, axis=0)
    grad_y = sobel(image, axis=1)

    # Smooth the gradients with a Gaussian filter
    grad_x = gaussian_filter(grad_x, sigma)
    grad_y = gaussian_filter(grad_y, sigma)

    # Compute the gradient magnitude
    grad_magnitude = np.sqrt(grad_x**2 + grad_y**2)

    # Compute the inverse Gaussian gradient
    inv_gaussian_grad = 1.0 / (1.0 + grad_magnitude)

    # Threshold to create a binary image

    return inv_gaussian_grad


def waershed_segmentation(extent_mask, scaled_image, kernel):
    """
    Process an image to remove noise, identify sure background and foreground areas,
    and apply watershed algorithm to segment the image.

    Parameters:
    extent_mask (numpy.ndarray): The binary mask of the image.
    scaled_image (numpy.ndarray): The image to be segmented.

    Returns:
    numpy.ndarray: The markers after applying the watershed algorithm.
    """

    # Noise removal

    opening = cv2.morphologyEx(extent_mask, cv2.MORPH_OPEN, kernel, iterations=4)

    # Sure background area
    sure_bg = cv2.dilate(opening, kernel, iterations=3)

    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
    ret2, sure_fg = cv2.threshold(dist_transform, 0.2 * dist_transform.max(), 255, 0)
    sure_fg = np.uint8(sure_fg)  # Ensure sure_fg is uint8

    # Finding unknown region
    unknown = sure_bg - sure_fg

    # Marker labelling
    ret, markers = cv2.connectedComponents(sure_fg)

    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1

    # Now, mark the region of unknown with zero
    markers[unknown == 1] = 0

    # Apply watershed algorithm
    markers = cv2.watershed(scaled_image, markers)

    return markers
