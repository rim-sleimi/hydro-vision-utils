from typing import Dict
import numpy as np
import geopandas as gpd
from rasterio.features import rasterize
from rasterio.transform import from_bounds
from .geometry_transformations import transform_geometry


def rasterize_geometries(gdf: gpd.GeoDataFrame, raster_meta: Dict) -> np.ndarray:
    """
    Rasterize geometries from a GeoDataFrame to create a mask.

    Args:
        gdf (gpd.GeoDataFrame): GeoDataFrame containing geometries to rasterize.
        raster_meta (dict): Raster metadata.

    Returns:
        np.ndarray: Rasterized mask as a NumPy array.
    """

    raster_bounds = raster_meta["bounds"]
    transform = from_bounds(
        raster_bounds.left,  # minx
        raster_bounds.bottom,  # miny
        raster_bounds.right,  # maxx
        raster_bounds.top,  # maxy
        raster_meta["width"],  # width in pixels
        raster_meta["height"],  # height in pixels
    )

    if gdf.crs != raster_meta["crs"]:
        print("they are different")
        gdf = gdf.to_crs(raster_meta["crs"], inplace=True)

    # Apply the transformation to each geometry in the GeoDataFrame
    gdf_transformed = gdf.copy()
    gdf_transformed.geometry = gdf.geometry.apply(
        lambda x: transform_geometry(x, transform)
    )

    extent_kwargs = {
        "out_shape": (raster_meta["height"], raster_meta["width"]),
        "transform": transform,
        "dtype": "uint8",
        "fill": 0,
        "all_touched": False,
    }
    return rasterize(
        [(geom, 1) for geom in gdf_transformed["geometry"]], **extent_kwargs
    )
