import cv2
import numpy as np
import pandas as pd
from typing import Union, Dict, Tuple, List
from skimage.color import rgb2hsv
from skimage.measure import find_contours
from scipy.ndimage import binary_erosion, distance_transform_edt
from HydroVisionUtils.src.utils.geometry_transformations import get_contours


def pad_to_divisible_with_overlap(
    image: np.ndarray, patch_size: int
) -> Tuple[np.ndarray, Tuple[int, int]]:
    """
    Pad image to be divisible by patch size.

    Args:
        image (np.ndarray): Input image array.
        patch_size (int): Patch size for padding.

    Returns:
        Tuple[np.ndarray, Tuple[int, int]]: Padded image and padding dimensions.
    """

    _, height, width = image.shape

    # Calculate the amount of padding needed for height and width
    pad_height = (patch_size - height % patch_size) % patch_size
    pad_width = (patch_size - width % patch_size) % patch_size
    # Apply padding
    padded_image = np.pad(
        image,
        (
            (0, 0),  # No padding for channels
            (0, pad_height),  # Padding for height
            (0, pad_width),  # Padding for width
        ),
        mode="constant",
        constant_values=0,
    )

    return padded_image, (0, pad_height, 0, pad_width)


def get_mesh_grid(raster: np.ndarray, patch_size: int, overlap: bool) -> np.ndarray:
    """
    Create a mesh grid for patches.

    Args:
        raster (np.ndarray): Input raster array.
        patch_size (int): Size of each patch.
        overlap (bool): Whether patches should overlap.

    Returns:
        np.ndarray: List of patch coordinates.
    """
    if (raster.shape[1] % patch_size != 0) or (raster.shape[2] % patch_size != 0):
        raise ValueError("Raster size must be divisible by patch size")
    step_size = 64 if overlap else patch_size
    raster_height, raster_width = raster.shape[1], raster.shape[2]

    xs = np.arange(0, raster_width - patch_size + step_size, step_size, dtype=int)
    ys = np.arange(0, raster_height - patch_size + step_size, step_size, dtype=int)

    mesh = np.array(np.meshgrid(ys, xs)).T.reshape(-1, 2)

    return mesh.tolist()


def compute_normalize_distance_transform(mask: np.ndarray) -> np.ndarray:
    """
    Compute the normalized distance transform of a binary mask to the range [0, 1].

    Args:
        mask (np.ndarray): Input binary mask.

    Returns:
        np.ndarray: Normalized distance transform of the input mask.
    """
    distance_map = distance_transform_edt(mask)
    max_distance = np.max(distance_map)
    return distance_map / max_distance


def create_boundary_mask(mask: np.ndarray) -> np.ndarray:
    """
    Create a boundary mask by dilating and eroding the input mask.

    Args:
        mask (np.ndarray): Input binary mask.

    Returns:
        np.ndarray: Boundary mask.
    """
    return binary_erosion(mask, border_value=1) ^ mask


def check_image_format(image: np.ndarray):
    if image.ndim != 3:
        raise ValueError("Input tensor must be 3-dimensional (C, H, W) or (H, W, C).")

    # Check the dimensions to determine if it's channel-first or channel-last
    if image.shape[0] <= 10 and image.shape[1] > 10 and image.shape[2] > 10:
        return "CHW"  # Channel-First
    elif image.shape[2] <= 10 and image.shape[0] > 10 and image.shape[1] > 10:
        return "HWC"  # Channel-Last
    else:
        raise ValueError(
            "The input tensor does not match typical image dimensions for CHW or HWC formats."
        )


def standardize_rgb_image(image: np.ndarray, stats: pd.DataFrame) -> np.ndarray:
    """
    Standardize an RGB image based on precomputed medians and standard deviations.

    Args:
        image (np.ndarray): Input RGB image tensor of shape (C, H, W).

    Returns:
        np.ndarray: Standardized RGB image tensor of the same shape.
    """

    medians = stats.values[0]
    stds = stats.values[1]
    if check_image_format(image) == "CHW":
        return (image - medians[:, None, None]) / stds[:, None, None]
    elif check_image_format(image) == "HWC":
        return (image.transpose(2, 0, 1) - medians[:, None, None]) / stds[:, None, None]


def convert_to_hsv(rgb_data: np.ndarray) -> np.ndarray:
    return rgb2hsv(rgb_data[:3, ...].transpose((1, 2, 0)))


def find_and_draw_contours(boundary_segmentation: np.ndarray) -> np.ndarray:
    contours = find_contours(
        boundary_segmentation, 0.5, fully_connected="high", positive_orientation="low"
    )
    res = get_contours(contours)
    blank_canvas = np.zeros_like(boundary_segmentation, dtype=np.uint8)
    for contour in res:
        contour[:, :, 0], contour[:, :, 1] = contour[:, :, 1], contour[:, :, 0].copy()
        cv2.drawContours(blank_canvas, [contour], -1, 255, 1)
    return blank_canvas


def apply_morphological_operation(
    binary_image: np.ndarray,
    operation: str,
    kernel_size: Tuple[int, int] = (3, 3),
    iterations: int = 1,
) -> np.ndarray:
    kernel = np.ones(kernel_size, dtype=np.uint8)

    if operation == "closing":
        closed_image = cv2.morphologyEx(
            binary_image, cv2.MORPH_CLOSE, kernel, iterations=iterations
        )
        return closed_image

    elif operation == "erosion":
        eroded_image = cv2.erode(binary_image, kernel, iterations=iterations)
        return eroded_image

    elif operation == "dilation":
        dilated_image = cv2.dilate(binary_image, kernel, iterations=iterations)
        return dilated_image

    else:
        raise ValueError(
            f"Unsupported operation: {operation}. Supported operations are 'closing', 'erosion', and 'dilation'."
        )
