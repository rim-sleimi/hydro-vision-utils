import json
import yaml
import logging
from pathlib import Path
from typing import Union, Dict, Tuple, List, Any, Optional
import torch
import numpy as np
import pandas as pd
import geopandas as gpd
import tifffile
from PIL import Image
import rasterio
from rasterio.transform import from_bounds
from rasterio.windows import Window
from HydroVisionUtils.src.utils.geometry_transformations import transform_geometry
from HydroVisionUtils.src.utils.python_utils import create_directory


## Json input - outputs
def load_json(file_path: Union[str, Path]) -> Dict[str, any]:
    """
    Load JSON data from a file.

    Args:
    - file_path (Union[str, Path]): Path to the JSON file to be loaded.

    Returns:
    - dict: A Python dictionary representing the JSON data.

    Raises:
    - FileNotFoundError: If the specified file_path does not exist.
    - JSONDecodeError: If the JSON data cannot be decoded.

    """
    with open(file_path, "r") as f:
        return json.load(f)


def json_to_geojson(data: List[Dict[str, Any]]) -> Dict[str, Any]:
    """
    Converts a list of dictionaries in a specified format to GeoJSON.

    Args:
        data (List[Dict[str, Any]]): A list of dictionaries containing 'class' and 'segmentation' keys.

    Returns:
        Dict[str, Any]: A GeoJSON FeatureCollection.

    Raises:
        ValueError: If segmentation data length is not even.
    """
    features = []
    for item in data:
        polygon = []
        # Ensure segmentation data length is even
        if len(item["segmentation"]) % 2 != 0:
            raise ValueError("Segmentation data length must be even.")
        # Extract polygon coordinates
        for i in range(0, len(item["segmentation"]), 2):
            polygon.append([item["segmentation"][i], item["segmentation"][i + 1]])
        # Append feature to the features list
        features.append(
            {
                "type": "Feature",
                "geometry": {"type": "Polygon", "coordinates": [polygon]},
                "properties": {"class": item["class"]},
            }
        )
    return {"type": "FeatureCollection", "features": features}


def geojson2gdf(geojson_dict: Dict[str, Any]) -> gpd.GeoDataFrame:
    """
    Converts a GeoJSON dictionary to a GeoDataFrame.

    Args:
        geojson_dict (Dict[str, Any]): A GeoJSON dictionary.

    Returns:
        gpd.GeoDataFrame: A GeoDataFrame containing the geometries and properties from the GeoJSON dictionary.
    """
    return gpd.GeoDataFrame.from_features(geojson_dict["features"])


def gdf2geojson(
    gdf: gpd.GeoDataFrame,
    output_dir: str,
    file_name: str,
    driver: str = "GeoJSON",
) -> None:
    """
    Convert a GeoDataFrame to GeoJSON (by default) format and save it to a file.

    Parameters:
    - gdf (gpd.GeoDataFrame): The GeoDataFrame to be converted to GeoJSON.
    - output_dir (str): The directory where the GeoJSON will be saved.
    - file_name (str): Name of the file that will be saved.
    - driver (str, optional): The OGR format driver to use for saving the file. Default is "GeoJSON".

    Returns:
    - None

    Raises:
    - ValueError: If the provided driver is not supported by GeoPandas.
    """
    create_directory(output_dir)
    gdf.to_file(output_dir / file_name, driver=driver)
    logging.info(f"Saving {file_name} to {output_dir}")


# Raster input - output
def load_raster(
    image_path: Union[str, Path],
    band_indices: list = None,
    window: Optional[Window] = None,
    return_meta: bool = False,
) -> Tuple[Dict[str, Any], np.ndarray]:
    """
    Read raster data and metadata from a raster file using rasterio.

    Args:
    - image_path (Union[str, Path]): Path to the raster file.
    - band_indices (list): List of band indices to read.
    - window (Optional[Window]): Optional window to read a subset of the raster data.

    Returns:
    - tuple: A tuple containing:
      - dict: Dictionary containing the following metadata:
        - 'meta': Metadata of the raster dataset (src.meta).
        - 'crs': Coordinate reference system of the raster dataset (src.crs).
        - 'bounds': Bounding box coordinates of the raster dataset (window bounds if provided, else src.bounds).
        - 'width': Width of the raster dataset or window.
        - 'height': Height of the raster dataset or window.
      - np.ndarray: Array containing the raster data for specified bands and window.

    Raises:
    - rasterio.errors.RasterioIOError: If the raster file cannot be opened or read.
    """
    with rasterio.open(image_path) as src:
        if return_meta:
            if window:
                transform = src.window_transform(window)
                bounds = rasterio.windows.bounds(window, transform)
                width, height = window.width, window.height
                meta = src.meta.copy()
                meta.update({"width": width, "height": height, "transform": transform})
                raster_data = src.read(band_indices, window=window)
            else:
                crs = src.crs
                transform = src.transform
                bounds = src.bounds
                width, height = src.width, src.height
                meta = src.meta
                raster_data = src.read(band_indices)

            return {
                "meta": meta,
                "transform": transform,
                "crs": crs,
                "bounds": bounds,
                "width": width,
                "height": height,
            }, raster_data
        else:
            return src.read(band_indices)


def write_raster(
    output_path: Union[str, Path],
    data: np.ndarray,
    dtype: type,
    raster_meta: Dict[str, Any],
):
    """
    Write a NumPy array as a GeoTIFF raster file.

    Args:
        output_path (Union[str, Path]): Path to save the output GeoTIFF file.
        data (np.ndarray): NumPy array to write as raster data.
        raster_meta (dict): Raster metadata.
    """
    if isinstance(data, torch.Tensor):
        data = data.detach().cpu().numpy().astype(dtype)
    elif not isinstance(data, np.ndarray):
        raise TypeError(
            f"Unsupported type for 'array': {type(data)}. Must be np.ndarray or torch.Tensor."
        )
    transform = from_bounds(
        raster_meta["bounds"].left,  # minx
        raster_meta["bounds"].bottom,  # miny
        raster_meta["bounds"].right,  # maxx
        raster_meta["bounds"].top,  # maxy
        raster_meta["width"],  # width in pixels
        raster_meta["height"],  # height in pixels
    )

    with rasterio.open(
        output_path,
        "w",
        driver="GTiff",
        width=raster_meta["width"],
        height=raster_meta["height"],
        count=1,
        dtype=dtype,
        crs=raster_meta["crs"],
        transform=transform,
    ) as dst:
        dst.write(data)


## tifffile input - outputs
def save_image(
    array: Union[np.ndarray, torch.Tensor],
    dtype: type,
    idx: int,
    output_dir: Union[str, Path],
    filename_prefix: str,
) -> None:
    """
    Save a numpy array or tensor as an image file.

    Parameters:
    - array (Union[np.ndarray, torch.Tensor]): The image data to save.
    - dtype (type): The data type to of the array.
    - idx (int): The index to include in the filename.
    - output_dir (Union[str, Path]): The directory to save the image in.
    - filename_prefix (str): The prefix for the filename.
    """

    if isinstance(array, torch.Tensor):
        array = array.detach().cpu().numpy().astype(dtype)
    elif not isinstance(array, np.ndarray):
        raise TypeError(
            f"Unsupported type for 'array': {type(array)}. Must be np.ndarray or torch.Tensor."
        )

    file_path = output_dir / f"{filename_prefix}_{idx}.tif"

    try:
        tifffile.imwrite(file_path, array)
        logging.info(f"Saved image: {file_path}")
    except Exception as e:
        logging.info(f"Failed to save image {file_path}: {e}")


# CSV input - output
def load_csv(stats_path: str) -> pd.DataFrame:
    return pd.read_csv(stats_path)


def save_statistics_to_csv(
    medians: Dict[str, float], stds: Dict[str, float], output_path: Path
) -> None:
    df = pd.DataFrame({"Median": medians, "Std": stds}).transpose()
    df.to_csv(output_path, index=False)


# YAML input - output
def load_config(config_file: str) -> Dict:
    """
    Loads a YAML configuration file and returns it as a dictionary.

    Args:
    - config_file (str): Path to the YAML configuration file.

    Returns:
    - dict: Configuration parameters loaded from the YAML file.
    """
    with open(config_file, "r") as f:
        config = yaml.safe_load(f)
    return config
