import cv2
import numpy as np
import geojson
import geopandas as gpd
from typing import Dict, List, Tuple, Union, Any
from shapely.geometry.base import BaseGeometry
from shapely.geometry import (
    Polygon,
    MultiPolygon,
    GeometryCollection,
    shape,
    LineString,
    LinearRing,
)
from shapely.affinity import affine_transform
from shapely.validation import make_valid
from skimage.measure import find_contours
from rasterio.transform import from_bounds
import matplotlib.pyplot as plt


def get_bounding_box(
    geometry: Union[
        geojson.Point,
        geojson.MultiPoint,
        geojson.LineString,
        geojson.MultiLineString,
        geojson.Polygon,
        geojson.MultiPolygon,
    ]
) -> Tuple[float, float, float, float]:
    """
    Compute the bounding box coordinates from a GeoJSON geometry.

    Args:
        geometry (Union[geojson.Point, geojson.MultiPoint, geojson.LineString, geojson.MultiLineString, geojson.Polygon, geojson.MultiPolygon]):
            Geometry object from the GeoJSON library representing the shape.

    Returns:
        Tuple[float, float, float, float]: Tuple containing (min_x, max_x, min_y, max_y) coordinates of the bounding box.
    """
    coords = np.array(list(geojson.utils.coords(geometry)))
    return (
        float(coords[:, 0].min()),
        float(coords[:, 0].max()),
        float(coords[:, 1].min()),
        float(coords[:, 1].max()),
    )


def transform_geometry(
    geom: Union[Polygon, MultiPolygon], transform
) -> Union[Polygon, MultiPolygon]:
    """
    Apply an affine transformation to a Shapely Polygon or MultiPolygon.

    Args:
    - geom (Union[Polygon, MultiPolygon]): Shapely geometry (Polygon or MultiPolygon) to transform.
    - transform: Affine transformation parameters provided by rasterio's transform object.

    Returns:
    - Union[Polygon, MultiPolygon]: Transformed geometry after applying the affine transformation.

    Raises:
    - ValueError: If the geometry type is neither Polygon nor MultiPolygon.

    """
    if geom.geom_type == "Polygon":
        return affine_transform(
            geom,
            [
                transform.a,
                transform.b,
                transform.d,
                transform.e,
                transform.xoff,
                transform.yoff,
            ],
        )
    elif geom.geom_type == "MultiPolygon":
        # Apply transformation to each polygon in the MultiPolygon
        transformed_polygons = [
            affine_transform(
                poly,
                [
                    transform.a,
                    transform.b,
                    transform.d,
                    transform.e,
                    transform.xoff,
                    transform.yoff,
                ],
            )
            for poly in geom.geoms
        ]
        return MultiPolygon(transformed_polygons)
    else:
        raise ValueError("Unsupported geometry type. Expected Polygon or MultiPolygon.")


def geometries_from_geojson(
    geojson_dict: Dict[str, Union[str, List[Dict[str, Union[str, dict]]]]]
) -> List[GeometryCollection]:
    """
    Extracts geometries from a GeoJSON dictionary.

    Args:
        geojson_dict (Dict[str, Union[str, List[Dict[str, Union[str, dict]]]]]): A dictionary in GeoJSON format.

    Returns:
        List[GeometryCollection]: A list of Shapely GeometryCollection objects representing the geometries extracted from the GeoJSON.

    Notes:
        - Each geometry in the GeoJSON dictionary is converted to a Shapely geometry object using `shapely.geometry.shape`.
        - If a geometry in the GeoJSON is not valid, it may raise a `ValueError`.

    """
    features = geojson_dict.get("features", [])
    geometries = [
        shape(feature["geometry"])
        for feature in features
        if feature["geometry"] is not None
    ]
    return geometries


## this is probably a useless function
def validate_geometry(
    geom: Union[Polygon, MultiPolygon]
) -> Union[Polygon, MultiPolygon]:
    """
    Clean and validate a Shapely geometry.

    Args:
        geom (Union[Polygon, MultiPolygon]): The Shapely geometry to clean and validate.

    Returns:
        Union[Polygon, MultiPolygon]: The cleaned and validated geometry.

    Notes:
        - If the geometry is not valid (`geom.is_valid` is False), it is repaired using `shapely.validation.make_valid`.
        - Returns the original geometry if it is already valid.

    """
    if not geom.is_valid:
        return make_valid(geom)
    return geom


def get_contours(
    contours: List[np.ndarray], apply_approximation: bool = False
) -> List[np.ndarray]:
    """
    Process contours and approximate polygons.

    Args:
    - contours (List[np.ndarray]): List of contours to process.
    - apply_approximation (bool, optional): Flag to apply polygon approximation (default: False).

    Returns:
    - List[np.ndarray]: List of processed contours or approximated polygons.
    """
    res: List[np.ndarray] = []

    for contour in contours:
        contour = np.squeeze(contour).astype(np.int32)

        if contour.ndim == 1:
            continue  # Skip single point contours

        hull = cv2.convexHull(contour)

        if apply_approximation:
            peri = cv2.arcLength(hull, closed=True)
            epsilon = 1e-10 * peri
            approx = cv2.approxPolyDP(hull, epsilon, closed=True)
            res.append(approx)
        else:
            if len(contour) > 10:
                res.append(contour)

    return res


# def process_contours(boundary_segmentation: np.ndarray) -> np.ndarray:
#     binary_contours = np.zeros_like(boundary_segmentation, dtype=np.uint8)
#     contours = find_contours(
#         np.flipud(boundary_segmentation),
#         0.5,
#         fully_connected="high",
#         positive_orientation="low",
#     )
#     fig, ax = plt.subplots(1, 1, figsize=(6, 6))
#     ax.imshow(np.flipud(boundary_segmentation), cmap="gray")
#     for contour in contours:
#         contour = contour[:, [1, 0]].astype(np.int32)
#         area = cv2.contourArea(contour)
#         if area < 100:
#             continue

#         cv2.drawContours(binary_contours, [contour], -1, 255, 1)

#         # Plotting each contour
#         ax.plot(contour[:, 0], contour[:, 1], linewidth=2)
#         plt.pause(0.1)  # Pause to display the plot step by step
#     plt.show()
#     return binary_contours


def process_contours(
    boundary_segmentation: np.ndarray, threshold: float = 100
) -> np.ndarray:
    binary_contours = np.zeros_like(boundary_segmentation, dtype=np.uint8)
    contours = find_contours(
        np.flipud(boundary_segmentation),
        0.5,
        fully_connected="high",
        positive_orientation="low",
    )

    fig, (ax_before, ax_after) = plt.subplots(1, 2, figsize=(12, 6))

    ax_before.imshow(np.flipud(boundary_segmentation), cmap="gray")
    ax_before.set_title("Contours Before Drawing Lines")

    ax_after.imshow(np.flipud(boundary_segmentation), cmap="gray")
    ax_after.set_title("Contours After Drawing Lines")

    for contour in contours:
        if len(contour) < 2:
            continue

        contour = contour[:, [1, 0]].astype(np.int32)
        area = cv2.contourArea(contour)
        if area < 100:
            continue

        start_point = tuple(contour[0])
        end_point = tuple(contour[-1])

        # Check if the points are correctly formed
        if len(start_point) != 2 or len(end_point) != 2:
            print(
                f"Invalid point shape: start_point={start_point}, end_point={end_point}"
            )
            continue

        distance = euclidean_distance(start_point, end_point)

        cv2.drawContours(binary_contours, [contour], -1, 255, 1)

        # Plotting each contour before drawing lines
        ax_before.plot(contour[:, 0], contour[:, 1], linewidth=2)

        if distance < threshold:
            # Draw a line between start_point and end_point
            cv2.line(binary_contours, start_point, end_point, 255, 1)
            # Plot the line on ax_after
            ax_after.plot(
                [start_point[0], end_point[0]], [start_point[1], end_point[1]], "r-"
            )

        # Plotting each contour after drawing lines
        ax_after.plot(contour[:, 0], contour[:, 1], linewidth=2)

    plt.tight_layout()
    plt.show()

    return binary_contours


def fix_simplify_buffer_geometry(
    geom: BaseGeometry,
    simplify_tolerance: float = 0.5,  # 0.01
    buffer_distance: float = 0.5,  # -0.01
) -> BaseGeometry:
    """
    Fixes, simplifies, and applies a negative buffer to a geometry.

    Args:
        geom (shapely.geometry.base.BaseGeometry): The input geometry.
        simplify_tolerance (float): Tolerance for simplifying the geometry.
        buffer_distance (float): Distance for the negative buffer.

    Returns:
        shapely.geometry.base.BaseGeometry: The processed geometry.
    """
    # Fix invalid geometries
    valid_geom = make_valid(geom)

    # Simplify geometries
    simplified_geom = valid_geom.simplify(simplify_tolerance)

    # Apply a negative buffer
    buffered_geom = simplified_geom.buffer(buffer_distance)

    return buffered_geom


def create_geometries_from_contours(
    contours: List[np.ndarray], area_threshold: int = 100, image_height: int = 90
) -> List[Dict[str, Polygon]]:
    """
    Create geometries from contours.

    This function processes a list of contour arrays and creates a list of
    geometries as dictionaries containing an ID and a Polygon object.
    Only contours that form closed loops, are counter-clockwise oriented,
    and have an area greater than the specified threshold are included.

    Args:
        contours (List[np.ndarray]): A list of contour arrays where each contour
                                    is represented as a numpy array of points.
        area_threshold (int): The minimum area required for a polygon to be included
                            in the output. Default is 100.
        image_height (int): The height of the image to flip y-coordinates correctly.

    Returns:
        List[Dict[str, Polygon]]: A list of dictionaries, each containing
                                  an "id" and a "geometry" key where
                                  "id" is an integer and "geometry" is
                                  a Polygon object.
    """
    geometries: List[Dict[str, Polygon]] = []
    for idx, contour in enumerate(contours):
        contour = contour[:, [1, 0]].astype(np.int32)

        # Flip y-coordinates
        contour[:, 1] = image_height - contour[:, 1]
        line = LineString([(x, y) for (x, y) in contour])

        if line.is_closed:
            polygon = Polygon(line)

            if polygon.exterior.is_ccw and polygon.area > area_threshold:
                processed_geom = fix_simplify_buffer_geometry(polygon)
                geometries.append({"id": idx, "geometry": processed_geom})

    return geometries


def reproject_and_transform_geometries(
    geometries: List[Dict[str, Polygon]], reproj_metadata: Dict[str, Any]
) -> gpd.GeoDataFrame:
    """
    Reproject and transform a list of geometries to a new coordinate reference system (CRS)
    and adjust their bounds based on provided metadata.

    Parameters:
    - geometries (List[Dict[str, Polygon]]): List of geometries, where each geometry is represented
      by a dictionary containing a 'geometry' key with a Polygon object.
    - reproj_metadata (Dict[str, Any]): Metadata containing information necessary for reprojection
      and transformation, including 'crs', 'bounds', 'width', and 'height'.

    Returns:
    - gpd.GeoDataFrame: GeoDataFrame containing the transformed geometries.

    """
    gdf = gpd.GeoDataFrame(geometries, crs=4326)
    gdf = gdf.to_crs(crs=reproj_metadata["crs"])
    transform = from_bounds(
        reproj_metadata["bounds"].left,
        reproj_metadata["bounds"].top,
        reproj_metadata["bounds"].right,
        reproj_metadata["bounds"].bottom,
        reproj_metadata["width"],
        reproj_metadata["height"],
    )
    gdf_transformed = gdf.copy()
    gdf_transformed["geometry"] = gdf["geometry"].apply(
        lambda geom: transform_geometry(geom, transform)
    )
    return gdf_transformed


def extract_interior_rings(contours):
    interior_rings = []

    for idx, contour in enumerate(contours):
        # Construct the LinearRing for the interior ring
        interior_ring = LinearRing(contour)

        # Create a Polygon object using only the interior ring
        polygon = Polygon(interior_ring)

        # Append to the list of interior rings
        interior_rings.append({"id": idx, "geometry": polygon})

    return interior_rings


def euclidean_distance(point1, point2):
    return np.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)
