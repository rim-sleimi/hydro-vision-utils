import unittest
import torch
import torch.nn.functional as F
from segmentation import BaseSegmentationLoss


class TestBaseSegmentationLoss(unittest.TestCase):

    def setUp(self):
        # Initialize the BaseSegmentationLoss with mock parameters
        self.loss_func = BaseSegmentationLoss(
            class_indices=[0, 1, 2],
            gamma=1.5,
            dice_focal=True,
            boundary=False,
            tanimoto=False,
        )
        self.logits = torch.randn(1, 3, 256, 256)
        self.labels = torch.randint(0, 3, (1, 256, 256))  # Ensure labels are correct
        self.dist_maps = torch.randn(1, 256, 256)  # Example dist_maps
        self.alpha = 0.5  # Example alpha value

        # Convert labels to one-hot encoding
        self.labels_one_hot = (
            F.one_hot(self.labels, num_classes=3).to(torch.float32).permute(0, 3, 1, 2)
        )  # Convert to float32

    def test_loss_calculation_no_boundary(self):

        # Ensure the one-hot tensor is not sparse COO
        if self.labels_one_hot.is_sparse:
            self.labels_one_hot = self.labels_one_hot.to_dense()

        total_loss = self.loss_func(
            self.logits, self.labels_one_hot, self.dist_maps, self.alpha
        )

        # Assert that total_loss is a torch.Tensor and has the expected shape or value
        self.assertIsInstance(total_loss, torch.Tensor)

    def test_loss_calculation_with_boundary(self):

        total_loss = self.loss_func(
            self.logits, self.labels_one_hot, self.dist_maps, self.alpha
        )
        # Assert statements for your test
        # Assert that total_loss is a torch.Tensor and has the expected shape or value
        self.assertIsInstance(total_loss, torch.Tensor)

    def test_missing_inputs_boundary_loss(self):

        dist_maps = None

        # This should raise a ValueError because dist_maps is None
        with self.assertRaises(ValueError):
            self.loss_func(self.logits, self.labels, dist_maps, self.alpha)

        # This should also raise a ValueError because alpha is not provided
        with self.assertRaises(ValueError):
            self.loss_func(self.logits, self.labels, dist_maps=None, alpha=None)


if __name__ == "__main__":
    unittest.main()
