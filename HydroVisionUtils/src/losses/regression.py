from typing import List, Optional
import torch
import torch.nn as nn
from torchmetrics.regression import MeanSquaredError
from HydroVisionUtils.src.losses.loss_utils import BoundaryLoss
from HydroVisionUtils.src.utils.input_output_utils import load_config


class BaseRegressionLoss(nn.Module):
    """
    Base class for segmentation loss functions.

    Attributes:
        class_indices (List[int]): List of class indices to be considered.
        gamma (float): Focal loss parameter.
        boundary (bool): Whether to include boundary loss.
    """

    def __init__(self, **kwargs) -> None:
        super().__init__()
        self.class_indices: List[int] = kwargs.get("class_indices", list(range(1)))
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.loss_functions = nn.ModuleDict()

        # Initialize loss functions based on kwargs
        self._initialize_loss_functions(kwargs)
        print(f"Initialized {self.__class__.__name__} with {kwargs}")

    def _initialize_loss_functions(self, kwargs) -> None:
        """Initialize loss functions based on provided arguments."""
        # Initialize loss functions based on kwargs
        if kwargs.get("boundary", True):
            self.loss_functions["boundary"] = BoundaryLoss(
                class_indices=self.class_indices
            )

        self.loss_functions["mse"] = MeanSquaredError().to(self.device)

    def calculate_loss(
        self,
        logits: torch.Tensor,
        dist_maps: Optional[torch.Tensor] = None,
        alpha: Optional[float] = None,
    ) -> torch.Tensor:
        """
        Calculate the combined loss.

        Args:
            logits (torch.Tensor): Predicted logits for the distance maps.
            dist_maps (Optional[torch.Tensor]): Distance maps (required when boundary is True).
            alpha (Optional[float]): Weight for boundary loss (required when boundary is True).

        Returns:
            torch.Tensor: Total loss.
        """
        total_loss = 0
        for key, loss_func in self.loss_functions.items():
            if key == "boundary" and alpha is not None:
                total_loss += alpha * loss_func(logits, dist_maps)
            else:
                total_loss += loss_func(logits.reshape(-1), dist_maps.reshape(-1))

        return total_loss

    def forward(
        self,
        logits: torch.Tensor,
        dist_maps: torch.Tensor,
        alpha: float = 0.5,
    ) -> torch.Tensor:
        """
        Forward pass of the loss function.

        Args:
            logits (torch.Tensor): Predicted logits.
            dist_maps (Optional[torch.Tensor]): Distance maps (required when boundary is True).
            alpha (Optional[float]): Weight for boundary loss (required when boundary is True).

        Returns:
            total_loss (torch.float): Total loss.
        """
        if "boundary" in self.loss_functions and alpha is None:
            raise ValueError("alpha must be provided when boundary is True")
        if alpha is not None and not 0 <= alpha <= 1:
            raise ValueError("alpha must be between 0 and 1")

        return self.calculate_loss(logits, dist_maps, alpha).to(torch.float)


# Example usage:
if __name__ == "__main__":
    # Test BoundaryLoss
    boundary_loss = BoundaryLoss(class_indices=list(range(1)))
    logits = torch.randn(1, 2, 3, 3)
    dist_maps_tensor = torch.randn(1, 2, 3, 3)
    boundary_loss_result = boundary_loss(logits, dist_maps_tensor)
    print("BoundaryLoss:", boundary_loss_result.item())

    # Test the combined loss
    base_loss = BaseRegressionLoss()
    logits = torch.randn(1, 2, 3, 3)
    dist_maps_tensor = torch.randn(1, 2, 3, 3)
    total_loss = base_loss(logits, dist_maps_tensor, alpha=0.5)
    print("Total Loss:", total_loss.item())
