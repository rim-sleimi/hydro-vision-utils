import torch
import torch.nn as nn
from typing import List


class TanimotoDistanceLoss(nn.Module):
    """Implementation of the Tanimoto distance, which is a modified version of the Jaccard distance.

    Tanimoto = (|X & Y|)/ (|X|^2+ |Y|^2 - |X & Y|)
             = sum(|A*B|)/(sum(|A|^2)+sum(|B|^2)-sum(|A*B|))

    Implementation taken from
    https://github.com/feevos/resuneta/blob/145be5519ee4bec9a8cce9e887808b8df011f520/nn/loss/loss.py#L7
    """

    def __init__(
        self,
        smooth: float = 1.0e-5,
        from_logits: bool = False,
        class_weights: float = None,
        reduction: str = "mean",
        normalise: bool = False,
    ):
        """Tanimoto distance loss.

        :param smooth: Smoothing factor. Default is 1.0e-5.
        :type smooth: float
        :param from_logits: Whether predictions are logits or probabilities, defaults to True
        :type from_logits: bool
        :param class_weights: Array of class weights to be applied to loss. Needs to be of `n_classes` length
        :type class_weights: torch.Tensor
        :param reduction: Reduction to be used, defaults to 'mean'
        :type reduction: str, optional
        :param normalise: Whether to normalise loss by the number of positive samples in the class, defaults to `False`
        :type normalise: bool
        """

        super(TanimotoDistanceLoss, self).__init__()

        self.smooth = smooth
        self.from_logits = from_logits
        self.normalise = normalise
        self.reduction = reduction
        self.class_weights = class_weights

    def forward(self, y_true: torch.Tensor, y_pred: torch.Tensor) -> torch.Tensor:

        if self.from_logits:
            y_pred = torch.softmax(y_pred, dim=1)

        n_classes = y_true.shape[1]
        volume = (
            torch.mean(torch.sum(y_true, dim=(2, 3)), dim=0)
            if self.normalise
            else torch.ones(n_classes, dtype=torch.float32)
        )

        weights = torch.reciprocal(torch.square(volume))
        new_weights = torch.where(
            torch.isinf(weights), torch.zeros_like(weights), weights
        )
        weights = torch.where(
            torch.isinf(weights), torch.ones_like(weights) * new_weights.max(), weights
        )
        weights = weights.to("cuda:0")

        intersection = torch.sum(y_true * y_pred, dim=(2, 3))
        sum_ = torch.sum(y_true * y_true + y_pred * y_pred, dim=(2, 3))

        num_ = intersection * weights + self.smooth
        den_ = (sum_ - intersection) * weights + self.smooth

        tanimoto = num_ / den_

        loss = 1.0 - tanimoto

        if self.class_weights is not None:
            loss = loss * self.class_weights

        if self.reduction == "mean":
            loss = torch.mean(loss)
        elif self.reduction == "sum":
            loss = torch.sum(loss)

        return loss


class BoundaryLoss(nn.Module):
    """
    Boundary loss for segmentation tasks. Based on the paper: "Boundary loss for highly unbalanced segmentation" - https://arxiv.org/pdf/1812.07032.pdf

    Args:
        class_indices (List[int]): List of class indices to be considered for the boundary loss.
    """

    def __init__(self, class_indices: List[int]) -> None:
        self.class_indices = class_indices

    def __call__(self, logits: torch.Tensor, dist_maps: torch.Tensor) -> torch.float:
        """
        Compute boundary loss.

        Args:
            logits (torch.Tensor): Predicted logits.
            dist_maps (torch.Tensor): Distance maps.

        Returns:
            loss (torch.float): Boundary loss.
        """

        if logits.shape != dist_maps.shape:
            raise ValueError(
                f"Expected logits and dist_maps to have the same shape got: {logits.shape} and {dist_maps.shape}"
            )

        if not dist_maps.shape[1] >= len(self.class_indices):
            raise ValueError(
                f"Expected class indices to be in {list(range(dist_maps.shape[1]))} got: {self.class_indices}"
            )

        pc = logits[:, self.class_indices, ...].type(torch.float32)
        dc = dist_maps[:, self.class_indices, ...].type(torch.float32)

        multipled = torch.einsum("bkwh,bkwh->bkwh", pc, dc)
        loss = multipled.mean().to(torch.float)
        return loss
