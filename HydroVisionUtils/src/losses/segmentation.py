from typing import List
import torch
import torch.nn as nn
from monai.losses import DiceFocalLoss, TverskyLoss
from HydroVisionUtils.src.utils.input_output_utils import load_config


class BaseSegmentationLoss(nn.Module):
    """
    Base class for segmentation loss functions.

    Attributes:
        class_indices (List[int]): List of class indices to be considered.
        dice_focal (bool): Whether to include dice focal loss.
        tanimoto (bool): Whether to include tanimoto loss.
    """

    def __init__(self, **kwargs) -> None:
        super().__init__()
        self.class_indices: List[int] = kwargs.get("class_indices", list(range(1)))
        self.loss_functions = nn.ModuleDict()

        # Initialize loss functions based on kwargs
        self._initialize_loss_functions(kwargs)

        print(f"Initialized {self.__class__.__name__} with {kwargs}")

    def _initialize_loss_functions(self, kwargs) -> None:
        """Initialize loss functions based on provided arguments."""
        if kwargs.get("dice_focal", True):
            self.loss_functions["dice_focal"] = DiceFocalLoss(
                include_background=True,
                to_onehot_y=False,
                sigmoid=False,
                batch=True,
                reduction="mean",
                jaccard=False,
                gamma=kwargs.get("gamma", 2.0),
                lambda_dice=1,
                lambda_focal=1,
            )

        if kwargs.get("tanimoto", True):
            self.loss_functions["tanimoto"] = TverskyLoss(
                include_background=True,
                batch=True,
                sigmoid=False,
                alpha=1.0,
                beta=1.0,
            )

    def calculate_loss(
        self,
        logits: torch.Tensor,
        labels: torch.Tensor,
    ) -> torch.Tensor:
        """
        Calculate the combined loss.

        Args:
            logits (torch.Tensor): Predicted logits.
            labels (torch.Tensor): True labels.

        Returns:
            torch.Tensor: Total loss.
        """
        total_loss = 0
        for key, loss_func in self.loss_functions.items():
            total_loss += loss_func(logits, labels)

        return total_loss

    def forward(
        self,
        logits: torch.Tensor,
        labels: torch.Tensor,
    ) -> torch.Tensor:
        """
        Forward pass of the loss function.

        Args:
            logits (torch.Tensor): Predicted logits.
            labels (torch.Tensor): True labels.

        Returns:
            total_loss (torch.float): Total loss.
        """
        return self.calculate_loss(logits, labels).to(torch.float)


# Example usage:
if __name__ == "__main__":
    # Test BaseSegmentationLoss
    base_loss = BaseSegmentationLoss(dice_focal=True, tanimoto=True)
    logits = torch.randn(1, 10)
    labels = torch.randint(0, 2, (1, 10))
    total_loss = base_loss(logits, labels)
    print("Total Loss:", total_loss.item())
