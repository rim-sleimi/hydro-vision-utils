import unittest
import torch
import torch.nn as nn
from torchmetrics import MatthewsCorrCoef
from segmentation import BaseSegmentationMetrics


class TestBaseSegmentationMetrics(unittest.TestCase):
    def setUp(self):
        self.device = "cpu"
        self.metrics_dict = {"mcc": MatthewsCorrCoef(task="binary").to(self.device)}
        self.segmentation_metrics = BaseSegmentationMetrics(
            metrics=self.metrics_dict, device=self.device
        )
        self.tp = torch.tensor([10])
        self.fn = torch.tensor([5])
        self.fp = torch.tensor([3])

    def test_metrics_initialization(self):
        self.assertEqual(len(self.segmentation_metrics.metrics), 1)
        self.assertTrue("mcc" in self.segmentation_metrics.metrics)

    def test_compute_dice(self):
        expected_dice = torch.tensor([0.7143])
        dice_score = self.segmentation_metrics.compute_dice(self.tp, self.fn, self.fp)
        self.assertTrue(torch.allclose(dice_score, expected_dice, atol=1e-5))

    def test_compute_iou(self):
        expected_iou = torch.tensor([0.5556])
        iou_score = self.segmentation_metrics.compute_iou(self.tp, self.fn, self.fp)
        self.assertTrue(torch.allclose(iou_score, expected_iou, atol=1e-4))

    def test_compute_precision(self):
        expected_precision = torch.tensor([0.7692])
        precision_score = self.segmentation_metrics.compute_precision(self.tp, self.fp)
        self.assertTrue(torch.allclose(precision_score, expected_precision, atol=1e-5))

    def test_compute_recall(self):
        expected_recall = torch.tensor([0.6667])
        recall_score = self.segmentation_metrics.compute_recall(self.tp, self.fn)
        self.assertTrue(torch.allclose(recall_score, expected_recall, atol=1e-5))

    def test_add_custom_metric(self):
        def custom_metric(tp, tn, fp, fn):
            return torch.tensor(0.8)  # Just a placeholder for demonstration

        self.segmentation_metrics.add_metric("custom_metric", custom_metric)
        self.assertTrue("custom_metric" in self.segmentation_metrics.metrics)

    def test_forward_method(self):
        logits = torch.tensor([[0.6, 0.4], [0.3, 0.7]])
        labels = torch.tensor([[1, 0], [1, 1]])

        results = self.segmentation_metrics.forward(logits, labels)
        self.assertTrue("dice" in results)
        self.assertTrue("iou" in results)
        self.assertTrue("precision" in results)
        self.assertTrue("recall" in results)

        # Assuming mcc metric is used
        self.assertTrue("mcc" in results)
        self.assertAlmostEqual(results["mcc"].item(), 0.5773, places=4)

    # Add more tests as needed...


if __name__ == "__main__":
    unittest.main()
