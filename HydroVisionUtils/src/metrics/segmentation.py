from typing import Dict, Callable
import torch
import torch.nn as nn
from torchmetrics import Metric, MatthewsCorrCoef


class BaseSegmentationMetrics(nn.Module):
    """
    Computes various segmentation metrics such as Dice, IoU, Precision, Recall, and others.

    Args:
        metrics (Dict[str, Callable]): A dictionary of metric names and their corresponding functions.
        device (str): The device to run the metrics on (default is "cuda:0").


     Usage example:
        # Create an instance of SegmentationMetrics
        metrics = SegmentationMetrics(device="cuda:0")

        # Add the custom accuracy metric
        metrics.add_metric('accuracy', custom_accuracy)

    """

    def __init__(
        self,
        metrics: Dict[str, Callable[[torch.Tensor, torch.Tensor], torch.Tensor]] = None,
        device: str = "cuda:0",
    ) -> None:
        super().__init__()
        self.device = device
        self.metrics = metrics or {
            "mcc": MatthewsCorrCoef(task="binary").to(self.device)
        }

    @staticmethod
    def compute_dice(
        tp: torch.Tensor, fn: torch.Tensor, fp: torch.Tensor
    ) -> torch.Tensor:
        """
        Computes the dice score between the predictions (probabilities) and the ground truth (gt).

        Args:
            tp (torch.Tensor): True positives.
            fn (torch.Tensor): False negatives.
            fp (torch.Tensor): False positives.

        Returns:
            score_cls (torch.Tensor): Dice score for a single class, averaged over all samples (batch).
        """
        denom = 2 * tp + fn + fp
        score_cls = torch.where(
            denom != 0, (2 * tp) / denom, torch.tensor(0, dtype=torch.float)
        )
        return score_cls

    @staticmethod
    def compute_iou(
        tp: torch.Tensor, fn: torch.Tensor, fp: torch.Tensor
    ) -> torch.Tensor:
        """
        Computes the intersection over union score between the predictions (probabilities) and the ground truth (gt).

        Args:
            tp (torch.Tensor): True positives.
            fn (torch.Tensor): False negatives.
            fp (torch.Tensor): False positives.

        Returns:
            score_cls (torch.Tensor): IoU score for a single class, averaged over all samples (batch).
        """
        denom = tp + fn + fp
        score_cls = torch.where(
            denom != 0, tp / denom, torch.tensor(0, dtype=torch.float)
        )
        return score_cls

    @staticmethod
    def compute_precision(tp: torch.Tensor, fp: torch.Tensor) -> torch.Tensor:
        """
        Computes the Precision score.

        Args:
            tp (torch.Tensor): True positives.
            fp (torch.Tensor): False positives.

        Returns:
            score_cls (torch.Tensor): Precision score for a single class, averaged over all samples (batch).
        """
        denom = tp + fp
        score_cls = torch.where(
            denom != 0, tp / denom, torch.tensor(0, dtype=torch.float)
        )
        return score_cls

    @staticmethod
    def compute_recall(tp: torch.Tensor, fn: torch.Tensor) -> torch.Tensor:
        """
        Computes the Recall score.

        Args:
            tp (torch.Tensor): True positives.
            fn (torch.Tensor): False negatives.

        Returns:
            score_cls (torch.Tensor): Recall score for a single class, averaged over all samples (batch).
        """
        den_ = tp + fn
        score_cls = torch.where(
            den_ != 0, tp / den_, torch.tensor(0, dtype=torch.float)
        )
        return score_cls

    def add_metric(
        self, name: str, metric: Callable[[torch.Tensor, torch.Tensor], torch.Tensor]
    ):
        """
        Adds a custom metric to the metrics dictionary.

        Args:
            name (str): Name of the metric.
            metric (Callable): Metric function.
        """
        self.metrics[name] = metric

    def forward(
        self, logits: torch.Tensor, labels: torch.Tensor
    ) -> Dict[str, torch.Tensor]:
        """
        Forward pass of the SegmentationMetrics module.

        Args:
            logits (torch.Tensor): Model predictions/logits.
            labels (torch.Tensor): Ground truth labels.

        Returns:
            Dict[str, torch.Tensor]: Dictionary containing all metric scores.
        """

        if labels.shape != logits.shape:
            raise ValueError(
                "Shape mismatch: labels and logits must have the same shape"
            )

        p = (logits > 0.5).int()
        if not torch.all((labels == 0) | (labels == 1)):
            raise ValueError("Labels must be binary (0 or 1)")
        if not torch.all((p == 0) | (p == 1)):
            raise ValueError("Predictions must be binary (0 or 1)")

        tp = torch.sum((labels == 1) & (p == 1))
        tn = torch.sum((labels == 0) & (p == 0))
        fp = torch.sum((labels == 0) & (p == 1))
        fn = torch.sum((labels == 1) & (p == 0))

        results = {
            "dice": self.compute_dice(tp, fn, fp),
            "iou": self.compute_iou(tp, fn, fp),
            "precision": self.compute_precision(tp, fp),
            "recall": self.compute_recall(tp, fn),
        }

        for name, metric in self.metrics.items():
            if isinstance(metric, Metric):
                results[name] = metric(p, labels)
            else:
                results[name] = metric(tp, tn, fp, fn)

        return results
