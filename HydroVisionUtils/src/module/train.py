import gc
import time
import logging
import random
import numpy as np
import torch
import torch.nn as nn
from tqdm import tqdm
from pathlib import Path
import wandb
from typing import Union, List
from HydroVisionUtils.src.utils.input_output_utils import load_config
from HydroVisionUtils.src.utils.python_utils import create_directory


class Trainer(nn.Module):
    def __init__(self, config_file: Union[str | Path]):
        super(Trainer, self).__init__()
        self.config = load_config(config_file)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.setup_seed(self.config["seed"])
        self.model = self.initialize_model()
        self.optimizer = self.set_optimizer()
        self.initialize_wandb()

    def setup_seed(self, seed: int) -> None:
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        if torch.cuda.is_available():
            torch.cuda.manual_seed(seed)
            torch.cuda.manual_seed_all(seed)

    def get_dataloaders(self):
        """
        This method should be implemented by subclasses to provide the data loaders.
        """
        raise NotImplementedError(
            f"This method is not defined for {self.__class__.__name__}"
        )

    def initialize_model(self):
        """
        This method should be implemented by subclasses to initialize the specific model.
        """
        raise NotImplementedError(
            f"This method is not defined for {self.__class__.__name__}"
        )

    def load_pretrained_model(self, model: nn.Module):
        pretrained_model_path = self.config["pretrained_model_path"]
        if not pretrained_model_path:
            raise ValueError("Pretrained model path was not provided.")
        try:
            # logging.info(f"Loading pretrained model from: {pretrained_model_path}")
            print(f"Loading pretrained model from: {pretrained_model_path}")
            state_dict = torch.load(pretrained_model_path)["model_state_dict"]
            model.load_state_dict(state_dict)
            self.start_epoch, self.min_train_loss = self.parse_pretrained_filename(
                pretrained_model_path
            )
        except Exception as e:
            logging.error(f"Failed to load pretrained model: {e}")
            raise

    @staticmethod
    def parse_pretrained_filename(filepath: Union[str, Path]):

        try:
            start_epoch = filepath.split("/")[-1].split("_")[1].split("-")[1]
            min_train_loss = (
                filepath.split("/")[-1].split("_")[2].split(".pth")[0].split("-")[1]
            )

            return int(start_epoch), float(min_train_loss)
        except (IndexError, ValueError) as e:
            logging.error(f"Error parsing pretrained filename: {filepath}, {e}")
            raise ValueError(f"Invalid pretrained model filename format: {filepath}")

    def save_checkpoint(
        self, min_train_loss: float, epoch: int, top_checkpoints: List[float]
    ):
        model_name = self.model.__class__.__name__.lower()
        checkpoint_dir = Path(f"{model_name}_ckpts")
        create_directory(checkpoint_dir)
        torch.save(
            {
                "epoch": epoch,
                "model_state_dict": self.model.state_dict(),
                "optimizer_state_dict": self.optimizer.state_dict(),
            },
            checkpoint_dir / f"{model_name}_epoch-{epoch}_loss-{min_train_loss}.pth",
        )
        logging.info(f"{model_name} checkpoint saved at epoch {epoch}")

        # Check and remove the old checkpoint if we have more than 2
        if len(top_checkpoints) > 2:
            oldest_checkpoint_path = (
                checkpoint_dir
                / f"{model_name}_epoch-{top_checkpoints[-1][1]}_loss-{top_checkpoints[-1][0]}.pth"
            )

            if oldest_checkpoint_path.exists():
                logging.info(f"Removing {oldest_checkpoint_path}")
                oldest_checkpoint_path.unlink()

    def set_optimizer(self):
        """
        This method should be implemented by subclasses to initialize the specific model.
        """
        raise NotImplementedError(
            f"This method is not defined for {self.__class__.__name__}"
        )

    def get_criterion(self):
        """
        This method should be implemented by subclasses to define the loss criterion.
        """
        raise NotImplementedError(
            f"This method is not defined for {self.__class__.__name__}"
        )

    def get_metrics(self):
        """
        This method should be implemented by subclasses to define the metrics.
        """
        raise NotImplementedError(
            f"This method is not defined for {self.__class__.__name__}"
        )

    def initialize_wandb(self):
        wandb.login()
        wandb.init(
            project=self.config["project_name"],
            name=self.config["run_name"],
        )
        wandb.watch(self.model, log="all", log_freq=self.config["log_freq"])

    def cleanup(self):
        torch.cuda.empty_cache()
        gc.collect()

    def train(self, epochs: int):
        for epoch in range(epochs):
            self.model.train()
            train_loss = 0.0
            with tqdm(
                total=len(self.train_loader), desc=f"Training Epoch {epoch+1}/{epochs}"
            ) as pbar:
                for batch in self.train_loader:
                    loss = self.train_step(batch)
                    train_loss += loss
                    pbar.set_postfix({"loss": loss})
                    pbar.update(1)
            self.avg_train_loss = train_loss / len(self.train_loader)
            val_loss = self.validate()
            wandb.log(
                {
                    "train_loss": self.avg_train_loss,
                    "val_loss": val_loss,
                    "epoch": epoch,
                }
            )
            print(
                f"Epoch {epoch+1}/{epochs} || Train Loss: {self.avg_train_loss:.4f} || Validation Loss: {val_loss:.4f}"
            )
        self.cleanup()

    def train_step(self, batch):
        """
        Training step for a single batch.
        """
        start_time = time.time()
        self.optimizer.zero_grad()
        inputs, labels = batch
        inputs, labels = inputs.to(self.device), labels.to(self.device)
        outputs = self.model(inputs)
        loss = self.compute_loss(outputs, labels)
        metrics = self.computte_metrics(outputs, labels)
        loss.backward()
        self.optimizer.step()

        iteration_time = time.time() - start_time

        return loss, metrics
