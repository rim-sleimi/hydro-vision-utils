import pathlib
from setuptools import setup, find_packages

# The directory containing this file
HERE = pathlib.Path(__file__).parent
# The text of the README file
README = (HERE / "README.md").read_text()
setup(
    name="HydroVisionUtils",
    version="0.1",
    packages=find_packages(),
    description="Various utilities for deep learning projects",
    long_description=README,
    long_description_content_type="text/markdown",
    author="Rim Sleimi",
    author_email="rsleimi@hydrosat.com",
    license="Apache License 2.0",
    classifiers=[
        "Programming Language :: Python",
    ],
    python_requires=">=3.10",
)
